# Overview #
LumenPath iOS SDK is packaged as a [CocoaPod](http://cocoapods.org/) library. It provides a simple way to  interact with iBeacon sensors.

## Using the library ##
* Create an account at [LumenPath](https://beta.lumenpath.com)
* Create a new application
* Register a few iBeacon sensors
* Integrate the LumenPath SDK

## Integrate the LumenPath SDK ##
* Set up the podfile

In you iOS application Podfile, add the following dependencies 

```
pod 'AFNetworking',  '~> 2.0.0'
pod 'KeychainItemWrapper', '~> 1.2'
pod 'LPSensorLib', :git => 'https://bitbucket.org/lumenpath/lumenpath-ios-sdk.git', :tag => "v0.0.4"
```

* Application code

To use the LumenPath SDK, make the following changes in your iOS application delegate class.

```
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
...
LPBeaconManager *lpBeaconManager = [[LPBeaconManager sharedInstance] initWithBaseURL:@"https://beta.lumenpath.com"
                                              withApplicationId:@"<lumenpath_application_id>"
                                                     withSecret:@"<lumenpath_application_secret>"
                                              withCorrelationId:@"<crm_id_or_your_internal_user_identifier"
                                                   withDeviceId:nil];
}
```

If you want to receive iBeacon sensor active/inactive events, register a listener with the `LPBeaconManager` shared instance.

```
LPBeaconManager *lpBeaconManager = [LPBeaconManager sharedInstance];
[lpBeaconManager addListener:self];
```

* Sample Application code
```
#import "AppDelegate.h"
#import "LPBeaconManager.h"

@interface AppDelegate () <UITabBarControllerDelegate>
@end

@implementation AppDelegate {
    ....
    ....
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    //Other app initialization code.
    ....
    ....
    [self initializeLPBeaconManager];
    ....
    // setup push here
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    return YES;
}


- (void)initializeLPBeaconManager {
    LPBeaconManager *lpBeaconManager = [[LPBeaconManager alloc] initWithBaseURL:@"https://beta.lumenpath.com"
                                              withApplicationId:@"app_id"
                                                     withSecret:@"secret"
                                              withCorrelationId:@"correlation_id_1"
                                                   withDeviceId:nil];
    [lpBeaconManager addListener:self];
}

// Delegation methods
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:LPPushDeviceTokenKey];
    NSLog(@"Registered the following device token: %@", [self asHexString:deviceToken]);
    [[LPBeaconManager sharedInstance] setDeviceId:[self asHexString:deviceToken]];
    [[LPBeaconManager sharedInstance] updateRegistration];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"Error in registration. Error: %@", err);
}

- (NSString *)deviceToken {
    return [[NSUserDefaults standardUserDefaults] stringForKey:LPPushDeviceTokenKey];
}

- (NSString *)asHexString:(NSData *)data {
    /* Returns hexadecimal string of NSData. Empty string if data is empty.   */
    const unsigned char *dataBuffer = (const unsigned char *) [data bytes];
    if (!dataBuffer) {
        return [NSString string];
    }
    NSUInteger dataLength = [data length];
    NSMutableString *hexString = [NSMutableString stringWithCapacity:(dataLength * 2)];
    for (int i = 0; i < dataLength; ++i) {
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long) dataBuffer[i]]];
    }
    return [NSString stringWithString:hexString];
}

// LPBeaconManagerListener methods
/**
* This method is called when a beacon is active near the device. In this example,
* the LPBeaconManager is queried for additional data about the beacon and then 
* alert the user if the beacon has a tag "pdx".
*/
- (void)beaconManager:(id)beaconManager didBecomeActive:(NSArray *)beacons {
    //Block that is called on completion of the getSensor call.
    void (^completionBlock)(LPSensor *, BOOL) = ^(LPSensor *sensor, BOOL success) {
        if(success){
            LPLOG("Active Beacon:[%@]", sensor);
            LPLOG("Tags:[%@]", [sensor tags]);
            for(NSString *tag in [sensor tags]){
                if([tag isEqualToString: @"pdx"]){
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Portland Airport"
                                                                    message:@"Alaska checkin counter is in terminal 4."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
            }
        }else {
            LPLOG("Cannot find beacon:[%@]", sensor);
        }
    };
    LPBeaconManager *lpBeaconManager = [LPBeaconManager sharedInstance];
    for (LPSensor *sensor in beacons) {
        [lpBeaconManager getSensor:[sensor uuid]
                           andMajor:[[sensor major] integerValue]
                           andMinor:[[sensor minor] integerValue]
                        didComplete:completionBlock];
    }
}

- (void)beaconManager:(id)beaconManager didBecomeInactive:(NSArray *)beacons {
    void (^completionBlock)(LPSensor *, BOOL) = ^(LPSensor *sensor, BOOL success) {
        if(success){
            LPLOG("InActive Beacon:[%@]", sensor);
        }else {
            LPLOG("Cannot find beacon:[%@]", sensor);
        }
    };
    LPBeaconManager *lpBeaconManager = [LPBeaconManager sharedInstance];
    for (LPSensor *sensor in beacons) {
        [lpBeaconManager getSensor:[sensor uuid]
                           andMajor:[[sensor major] integerValue]
                           andMinor:[[sensor minor] integerValue]
                        didComplete:completionBlock];
    }
}

- (void)beaconManager:(id)beaconManager didChange:(NSArray *)beacons {
    for (LPSensor *sensor in beacons) {
        LPLOG(@"Changed beacon:%@", sensor);
    }
}

@end
```