//
//  LPClient.m
//  LumenPath
//
//  Created by Alok Singh on 5/19/14.
//

#import "LPGlobal.h"
#import "LPClient.h"
#import "AFHTTPRequestOperationManager.h"
#import <CoreLocation/CoreLocation.h>
#import "Reachability.h"

@implementation LPClient{
@private
    AFHTTPRequestOperationManager *_manager;
    NSString *_applicationId;
    NSString *_secret;
    NSString *_apiEndpoint;
    NSMutableDictionary *_headers;
}
- (id)initWithBaseURL:(NSString *)baseURL withApplicationId:(NSString *)applicationId withSecret:(NSString *)secret withHeaders:(NSDictionary *)headers{
    self = [super init];
    if(self){
        _applicationId = applicationId;
        _secret = secret;
        _apiEndpoint = baseURL;
        _manager = [AFHTTPRequestOperationManager manager];
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
//        _manager.credential = [NSURLCredential credentialWithUser:applicationId password:secret persistence:NSURLCredentialPersistenceForSession];
        _headers= [[NSMutableDictionary alloc] init];
        if(headers != nil){
            for(NSString *key in headers.allKeys){
                [self addHeader:key value: headers[key]];
            }
        }
        NSData *userPassword =  [[NSString stringWithFormat:@"%@:%@", applicationId, secret] dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64Encoded = [userPassword base64EncodedStringWithOptions:0];
        [self addHeader:@"Authorization" value: [NSString stringWithFormat:@"Basic %@", base64Encoded]];
        [self addHeader:LP_CLIENT_VERSION_HEADER value:[@(LP_CLIENT_VERSION) stringValue]];
    }
    return self;
}

- (BOOL)PUT:(NSString *)path withDictionary:(NSDictionary *)jsonParameters didComplete:(void (^)(NSDictionary *results, BOOL success))completion {
    if(![LPClient isNetworkAvailable]){
        NSLog(@"Network is unavaliable");
        completion(nil, FALSE);
        return FALSE;
    }
    NSString *fullPath = [self buildPath:path];
    [_manager PUT:fullPath parameters:jsonParameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
//              LPLOG(@"JSON: %@", responseObject);
              if(completion != nil){
                completion(responseObject, YES);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              LPLOG(@"Error: %@", error);
              if(completion != nil){
                completion(nil, NO);
              }
          }];
    return YES;
}

- (NSString *)buildPath:(NSString *)path {
    return [NSString stringWithFormat:@"%@/%@", _apiEndpoint, path];
}

- (void)GET:(NSString *)path withDictionary:(NSDictionary *)parameters didComplete:(void (^)(NSDictionary *, BOOL success))completion; {
    if(![LPClient isNetworkAvailable]){
        NSLog(@"Network is unavaliable");
        completion(nil, FALSE);
        return;
    }

    NSString *fullPath = [self buildPath:path];
    AFHTTPRequestOperation *requestOperation = [_manager GET:fullPath parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
//              LPLOG(@"JSON: %@", responseObject);
              if([responseObject isKindOfClass:[NSDictionary class]]){
                if(completion != nil){
                    completion(responseObject, YES);
                }
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              LPLOG(@"Error: %@", error);
              completion(nil, NO);
          }];
    [requestOperation waitUntilFinished];
}

- (BOOL)POST:(NSString *)path withDictionary:(NSDictionary *)jsonParameters didComplete:(void (^)(NSDictionary *results, BOOL success))completion {
    if(![LPClient isNetworkAvailable]){
        NSLog(@"Network is unavaliable");
        completion(nil, FALSE);
        return FALSE;
    }
    
    NSString *fullPath = [self buildPath:path];
    [_manager POST:fullPath parameters:jsonParameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
//              LPLOG(@"JSON: %@", responseObject);
              if(completion != nil){
                completion(responseObject, YES);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              LPLOG(@"Error: %@", error);
              if(completion != nil){
                completion(nil, NO);
              }
          }
     ];
    return YES;
}

- (BOOL)DELETE:(NSString *)path withDictionary:(NSDictionary *)parameters {
    if(![LPClient isNetworkAvailable]){
        NSLog(@"Network is unavaliable");
        return FALSE;
    }
    NSString *fullPath = [self buildPath:path];
    [_manager DELETE:fullPath parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              LPLOG(@"JSON: %@", responseObject);
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              LPLOG(@"Error: %@", error);
          }];
    return YES;
}

+(NSString *) dictionaryAsJson:(NSDictionary *)dictionary{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    if (!jsonData) {
        LPLOG(@"Got an error: %@", error);
        return nil;
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

- (void)addHeader:(NSString *)name value:(NSString *)value {
    if(value != nil){
        _headers[name] = value;
        [_manager.requestSerializer setValue:value forHTTPHeaderField:name];
    }
}

- (void)removeHeader:(NSString *)name{
    [_headers removeObjectForKey:name];
    [_manager.requestSerializer setValue:nil forHTTPHeaderField:name];
}

+(NSDictionary *) jsonAsDictionary:(NSString *)json{
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    return [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
}

+(NSArray *) jsonAsArray:(NSString *)json{
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    return [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
}

/***************************** UICOLOR FROM HEXADECIMAL STRING & VALUE **************************/

/***************************************** NETWORK STATUS  **************************************/
+(BOOL)isNetworkAvailable {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    return networkStatus==NotReachable?NO:YES;
}
/***************************************** NETWORK STATUS  **************************************/

/**************************************** LOCATION SERVICES *************************************/
+(BOOL)isLocationServiceEnabled {
    return [CLLocationManager locationServicesEnabled]?YES:NO;
}

+(BOOL)isLocationServicesEnabledForApp {
    return [CLLocationManager authorizationStatus]!=kCLAuthorizationStatusDenied?YES:NO;
}

@end
