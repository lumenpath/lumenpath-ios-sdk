//
//  LPClient.h
//  LumenPath
//
//  Created by Alok Singh on 5/19/14.
//  Copyright (c) 2014 LumenPath Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
* A convenience class to make calls to the LumenPath web service.
*/
static NSString *const LP_CLIENT_VERSION_HEADER = @"X-LP-CLIENT-VERSION";
static int LP_CLIENT_VERSION = 1000001;

@interface LPClient : NSObject

- (id)initWithBaseURL:(NSString *)baseURL
    withApplicationId:(NSString *)applicationId
           withSecret:(NSString *)secret
          withHeaders:(NSDictionary *)headers;

+ (NSString *)dictionaryAsJson:(NSDictionary *)dictionary;

+ (NSDictionary *)jsonAsDictionary:(NSString *)json;

+ (NSArray *)jsonAsArray:(NSString *)json;

- (BOOL)PUT:(NSString *)path withDictionary:(NSDictionary *)jsonParameters didComplete:(void (^)(NSDictionary *results, BOOL success))completion;

- (void)GET:(NSString *)path withDictionary:(NSDictionary *)parameters didComplete:(void (^)(NSDictionary *results, BOOL success))completion;;

- (BOOL)POST:(NSString *)path withDictionary:(NSDictionary *)jsonParameters didComplete:(void (^)(NSDictionary *results, BOOL success))completion;

- (BOOL)DELETE:(NSString *)path withDictionary:(NSDictionary *)parameters;

- (void)addHeader:(NSString *)name value:(NSString *)value;


+(BOOL)isNetworkAvailable;
+(BOOL)isLocationServiceEnabled;
+(BOOL)isLocationServicesEnabledForApp;

@end
