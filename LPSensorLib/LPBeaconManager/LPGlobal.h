/*
 Copyright 2009-2014 Lumenpath Inc. All rights reserved.
 */

#import <Foundation/Foundation.h>

typedef enum _LPLogLevel {
    LPLogLevelUndefined = -1,
    LPLogLevelNone = 0,
    LPLogLevelError = 1,
    LPLogLevelWarn = 2,
    LPLogLevelInfo = 3,
    LPLogLevelDebug = 4,
    LPLogLevelTrace = 5
} LPLogLevel;


#define LP_LEVEL_LOG_THREAD(level, levelString, fmt, ...) \
    do { \
        if (lpLoggingEnabled && lpLogLevel >= level) { \
            NSString *thread = ([[NSThread currentThread] isMainThread]) ? @"M" : @"B"; \
            NSLog((@"[%@] [%@] => %s [Line %d] " fmt), levelString, thread, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__); \
        } \
    } while(0)

#define LP_LEVEL_LOG_NO_THREAD(level, levelString, fmt, ...) \
    do { \
        if (lpLoggingEnabled && lpLogLevel >= level) { \
            NSLog((@"[%@] %s [Line %d] " fmt), levelString, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__); \
        } \
    } while(0)

//only log thread if #LP_LOG_THREAD is defined
//#ifdef LP_LOG_THREAD
#define LP_LEVEL_LOG LP_LEVEL_LOG_THREAD
//#else
//#define LP_LEVEL_LOG LP_LEVEL_LOG_NO_THREAD
//#endif

extern BOOL lpLoggingEnabled; // Default is true
extern LPLogLevel lpLogLevel; // Default is LPLogLevelError

#define LP_LTRACE(fmt, ...) LP_LEVEL_LOG(LPLogLevelTrace, @"T", fmt, ##__VA_ARGS__)
#define LP_LDEBUG(fmt, ...) LP_LEVEL_LOG(LPLogLevelDebug, @"D", fmt, ##__VA_ARGS__)
#define LP_LINFO(fmt, ...) LP_LEVEL_LOG(LPLogLevelInfo, @"I", fmt, ##__VA_ARGS__)
#define LP_LWARN(fmt, ...) LP_LEVEL_LOG(LPLogLevelWarn, @"W", fmt, ##__VA_ARGS__)
#define LP_LERR(fmt, ...) LP_LEVEL_LOG(LPLogLevelError, @"E", fmt, ##__VA_ARGS__)

#define LPLOG LP_LDEBUG
