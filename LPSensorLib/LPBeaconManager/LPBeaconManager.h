//
//  LPBeaconManager.h
//  LumenPath
//
//  Created by Alok Singh on 5/15/14.
//  Copyright (c) 2014 LumenPath Inc.
//  All rights reserved.
//

/**
* `LPBeaconManager` class is used to register an application instance with LumenPath service and
* to listen for iBeacon sensors. `LPBeaconManager` has convenience methods to query for
* iBeacon sensors based on a variety of criteria like location, tags, proximity to another sensor
* etc.
*
*/

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import "LPBeaconManagerListener.h"
#import "LPGlobal.h"
#import "LPSensor.h"
#import "LPAsset.h"
@class LPClient;


static NSString *const LP_DEVICE_ID_HEADER = @"X-LP-DEVICE-ID";
static NSString *const LP_DEVICE_TYPE_HEADER = @"X-LP-DEVICE-TYPE";
static NSString *const LP_DEVICE_CORRELATION_ID_HEADER = @"X-LP-DEVICE-CORRELATION-ID";
static NSString *const LP_DEVICE_LP_ID_HEADER = @"X-LP-DEVICE-LP-ID";
static NSString *const LP_DEVICE_APPLICATION_ID_HEADER = @"X-LP-DEVICE-APPLICATION-ID";
static NSString *const ERR_CODE_REG_1001 = @"REG_1001";
static NSString *const ERROR_CODE_KEY = @"error_code";
static NSString *const APPLICATION_ID_KEY = @"application_id";
static NSString *const CORRELATION_ID_KEY = @"correlation_id";
static NSString *const DEVICE_ID_KEY = @"device_id";
static NSString *const DEVICE_TYPE_KEY = @"device_type";
static NSString *const LP_ID_KEY = @"lp_id";
static NSString *const DEFAULT_SENSORS_KEY = @"default_sensors";

@interface LPBeaconManager : NSObject <LPBeaconManagerListener>

/**
* The period in seconds after the last activity for a iBeacon after which the mobile device
* is considered to have left the iBeacon region.
*/
@property(nonatomic, assign) NSInteger maximumInactivityPeriodInSeconds;


/**
 * The period in seconds during which beacon inactivity events will be sent to server.
 */
@property(nonatomic, assign) NSInteger maximumInactivityEventPeriodInSeconds;

/**
* The period in seconds after the last activity for a iBeacon after which
* data associated with the iBeacon is removed from the cache.
*/
@property(nonatomic, assign) NSInteger maximumInactiveWatchPeriodInSeconds;

/**
* The current coordinates of the device.
*/

@property(nonatomic) CLLocationCoordinate2D currentLocation;


/**
* Initializes an `LPBeaconManager` object with the specified base URL, applicationId, application secret,
* optional correlation id and an optional device identifier.
* @param url The base URL for the LumenPath service.
* @param applicationId application id
* @param secret application secret
* @param correlationId Correlation id used to associate LumenPath registration id with an external system of record. Can be nil
* @praam deviceId iOS device token, used to send push notifications to the device. Can be nil
* @return The newly-initialized LPBeaconManager instance
*/
- (id)initWithBaseURL:url
    withApplicationId:(NSString *)applicationId
           withSecret:(NSString *)secret
    withCorrelationId:(NSString *)correlationId
         withDeviceId:(NSString *)deviceId
       didComplete:(void (^)(NSString *msg, BOOL success))completion;

/**
* Registers a listener that will receive iBeacon activity events. See `LPBeaconManagerListener`
* @param listener A object that implements the `LPBeaconManagerListener` interface
*/
- (void)addListener:(id <LPBeaconManagerListener>)listener;

/**
*  Start monitoring an iBeacon
*  @param proximityUUID iBeacon uuid
*  @param major iBeacon major number
*  @param minor iBeacon minor number
*/

- (void)monitorRegion:(NSString *)proximityUUID andMajor:(NSInteger)major andMinor:(NSInteger)minor;

/**
*  Stop monitoring an iBeacon
*  @param proximityUUID iBeacon uuid
*  @param major iBeacon major number
*  @param minor iBeacon minor number
*/
- (void)stopRegionMonitoring:(NSString *)proximityUUID andMajor:(NSInteger)major andMinor:(NSInteger)minor;

/**
* Find all iBeacons near a given location.
* @param latitude latitude
* @param longitude longitude
* @param completion a block object to be executed upon receiving a response from LumenPath web service. This block takes an array of sensors and boolean indicating whether the remote call was successful.
*/
- (void)findSensorsByLocation:(double)latitude
                 andLongitude:(double)longitude
                  didComplete:(void (^)(NSArray *sensors, BOOL success))completion;

/**
* Find all iBeacons near a given address.
* @param country country. Optional, defaults to US if set to nill
* @param state state
* @param city city
* @param street street address. Can be nil
* @param completion a block object to be executed upon receiving a response from LumenPath web service. This block takes an array of sensors and boolean indicating whether the remote call was successful.
*/
- (void)findSensorsByCountry:(NSString *)country
                    andState:(NSString *)state
                     andCity:(NSString *)city
                   andStreet:(NSString *)street
                 didComplete:(void (^)(NSArray *sensors, BOOL success))completion;

/**
* Find all iBeacons near a given postal code.
* @param postalCode us postal code
* @param completion a block object to be executed upon receiving a response from LumenPath web service. This block takes an array of sensors and boolean indicating whether the remote call was successful.
*/
- (void)findSensorsByPostalCode:(NSString *)postalCode
                    didComplete:(void (^)(NSArray *sensors, BOOL success))completion;


/**
* Find all iBeacons near a given postal code.
* @param ns namespace to search in
* @param key key to search for
* @param value value to search for
* @param completion a block object to be executed upon receiving a response from LumenPath web service. This block takes an array of sensors and boolean indicating whether the remote call was successful.
 */
- (void)findSensorsByMetdata:(NSString *)ns
                      andKey:(NSString *)key
                    andValue:(NSString *)value
                 didComplete:(void (^)(NSArray *sensors, BOOL success))completion;

/**
* Get all the metadata associated with a given iBeacon sensor.
* @param uuid iBeacon UUID
* @param major iBeacon major
* @param minor iBeacon minor
* @param completion a block object to be executed upon receiving a response from LumenPath web service. This block takes a LPSensor and boolean indicating whether the remote call was successful.
*/
- (void)getSensor:(NSString *)uuid
         andMajor:(NSInteger)major
         andMinor:(NSInteger)minor
        didComplete:(void (^)(LPSensor *, BOOL))completion;

/**
 * Get all the asset collections associated with a given iBeacon sensor.
 * @param uuid iBeacon UUID
 * @param major iBeacon major
 * @param minor iBeacon minor
 * @param completion a block object to be executed upon receiving a response from LumenPath web service. This block takes a dictionary and boolean indicating whether the remote call was successful.
 */
- (void)getSensorAssets:(NSString *)uuid
         andMajor:(NSInteger)major
         andMinor:(NSInteger)minor
      didComplete:(void (^)(NSDictionary *, BOOL))completion;

/**
 * Get all the assets in a collection associated with a given iBeacon sensor.
 * @param uuid iBeacon UUID
 * @param major iBeacon major
 * @param minor iBeacon minor
 * @param collection collection name
 * @param completion a block object to be executed upon receiving a response from LumenPath web service. This block takes a array of LPAsset object and boolean indicating whether the remote call was successful.
 */
- (void)getSensorAssets:(NSString *)uuid
               andMajor:(NSInteger)major
               andMinor:(NSInteger)minor
          andCollection:(NSString *)collection
            didComplete:(void (^)(NSArray *, BOOL))completion;


/**
 * Get an asset associated with the application.
 * @param completion a block object to be executed upon receiving a response from LumenPath web service. This block takes a LPAsset object and boolean indicating whether the remote call was successful.
 */
- (void)getApplicationAsset:(NSString *)assetId didComplete:(void (^)(LPAsset *, BOOL))completion;


/**
 * Get all the asset collections associated with the application.
 * @param completion a block object to be executed upon receiving a response from LumenPath web service. This block takes a dictionary and boolean indicating whether the remote call was successful.
 */
- (void)getApplicationAssets:(void (^)(NSDictionary *, BOOL))completion;

/**
 * Get all the assets in a collection associated with the application.
 * @param collection collection name
 * @param completion a block object to be executed upon receiving a response from LumenPath web service. This block takes a array of LPAsset object and boolean indicating whether the remote call was successful.
 */
- (void)getApplicationAssets:(NSString *)collection didComplete:(void (^)(NSArray *, BOOL))completion;


/**
* Set the device token, used to send APNS notifications to the device. Call `updateRegistration` method to persist this data with LumenPath serivce.
* @param deviceId iOS device token
*/
- (void)setDeviceId:(NSString *)deviceId;

/**
* Adds a piece of namespaced metadata to be saved as part of device registration/update calls. Call `updateRegistration` method to persist this data with LumenPath serivce.
* @param namespace namespace/bucket to store the metadata 
* @param key key
* @value key value
*/

- (void)addMetadata:(NSString *)namespace withKey:(NSString *)key withValue:(NSString *)value;

/**
 * Returns a piece of namespaced metadata.
 * @param namespace namespace/bucket to retrieve the metadata from
 * @param key key
 */

- (NSString *)getMetadata:(NSString *)namespace withKey:(NSString *)key;


/**
* Saves the registration metadata with the LumenPath service.
*/
- (void)updateRegistration;

/**
* Clears all registration metadata on the device.
*/
- (void)clearMetadata;

/**
* Clears all registration data on the device
*/
- (void)clearRegistration;

/**
* Returns the correlation id
*/
- (NSString *)getCorrelationId;

/**
 * Set the correlation id. Call `updateRegistration` method to persist this data with LumenPath serivce.
 */

- (void)setCorrelationId:(NSString *)correlationId;


/**
* Returns the LPBeaconManager singleton instance. `initWithBaseURL...` must be first used to intialize the instance.
*/
+ (id)sharedInstance;


+ (BOOL) shouldActivateBeacon:(LPSensor *)sensor;
@end
