//
//  LPBeaconManager.m
//  LumenPath
//
//  Created by Alok Singh on 5/15/14.
//  Copyright (c) 2014 LumenPath Inc. All rights reserved.
//

#import <Foundation/NSLock.h>
#import <CoreLocation/CLLocationManager.h>
#import <CoreLocation/CLBeaconRegion.h>
#import "KeychainItemWrapper.h"
#import "LPBeaconManager.h"
#import "LPClient.h"
#import "LPEvent.h"
#import "LPEventQueue.h"
#import "LPAsset.h"

#define kDefaultMaxInactivityPeriodInSeconds 3
#define kDefaultMaxInactivityEventPeriodInSeconds 30
#define kDefaultMaxWatchPeriodInSeconds 900
#define kDefaultCacheExpiryInSeconds 300
#define kDefaultDelayBeforeSensorReactivationInSeconds 10
#define kDefaultSearchRadiusInMeters 500

BOOL lpLoggingEnabled = YES;
LPLogLevel lpLogLevel = LPLogLevelDebug;
typedef void (^CompletionHandler)(NSString *msg, BOOL success);
static dispatch_queue_t _remoteOperationsQueue;
static dispatch_queue_t _localOperationsQueue;

@interface LPBeaconManager () <CLLocationManagerDelegate>
@property(nonatomic, strong) CLLocationManager *locationManager;
- (NSString *)keyFromSensor:(LPSensor *)sensor;
- (NSString *)keyFromUUID:(NSString *)uuid andMajor:(NSInteger)major andMinor:(NSInteger)minor;
- (void) fetchSensor:(LPSensor *)sensor;
- (BOOL) hasCachedSensorExpired:(LPSensor *)sensor;
- (BOOL) isLocationUseAuthorized;
@end

@implementation LPBeaconManager {
@private
    NSMutableSet *_beaconsUnderWatch;
    NSMutableDictionary *_beaconCache;
    NSLock *_beaconsWatchLock;
    NSLock *_beaconsCacheLock;
    NSMutableArray *_delegates;
    LPClient *_lpClient;
    NSString *_applicationId;
    NSString *_correlationId;
    NSString *_deviceId;
    NSString *_lpId;
    LPEventQueue *_eventQueue;
    NSMutableDictionary *_metadata;
    CompletionHandler _registrationCompletionBlock;
}

- (void)dealloc {
    _delegates = nil;
    _beaconsUnderWatch = nil;
    _metadata = nil;
}

+(LPBeaconManager *)sharedInstance
{
    static LPBeaconManager *sharedInstance = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[LPBeaconManager alloc] init];
        _remoteOperationsQueue = dispatch_queue_create("com.lumenpath.beaconmanager.OpsQueue", DISPATCH_QUEUE_CONCURRENT  );
        _localOperationsQueue = dispatch_queue_create("com.lumenpath.beaconmanager.OpsQueue", DISPATCH_QUEUE_SERIAL);
    });
    
    return sharedInstance;
}

-(id) initWithBaseURL:url
    withApplicationId:(NSString *)applicationId
           withSecret:(NSString *)secret
    withCorrelationId:(NSString *)correlationId
         withDeviceId:(NSString *)deviceId
          didComplete:(CompletionHandler)completion{
    if ((self = [LPBeaconManager sharedInstance])){
        self.maximumInactivityPeriodInSeconds = kDefaultMaxInactivityPeriodInSeconds;
        self.maximumInactiveWatchPeriodInSeconds = kDefaultMaxWatchPeriodInSeconds;
        self.maximumInactivityEventPeriodInSeconds = kDefaultMaxInactivityEventPeriodInSeconds;
        
        if ([CLLocationManager locationServicesEnabled]){
            self.locationManager = [[CLLocationManager alloc] init];
            if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
                NSString *alwaysUse = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"];
                if(alwaysUse != nil){
                    [self.locationManager requestAlwaysAuthorization];
                }else{
                    NSString *inUse = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"];
                    if(inUse != nil){
                        [self.locationManager requestWhenInUseAuthorization];
                    }
                }
                
            }
            self.locationManager.delegate = self;
            if([self isLocationUseAuthorized]){
                [self.locationManager startUpdatingLocation];
            }
        }
        _applicationId = applicationId;
        _correlationId = correlationId;
        _deviceId = deviceId;
        _lpClient = [[LPClient alloc] initWithBaseURL:url
                                  withApplicationId:applicationId
                                         withSecret:secret
                                        withHeaders:nil];
        _beaconsWatchLock = [[NSLock alloc] init];
        _beaconsCacheLock = [[NSLock alloc] init];
        _beaconsUnderWatch = [[NSMutableSet alloc] initWithCapacity:100];
        _beaconCache = [[NSMutableDictionary alloc] init] ;
        _delegates = [[NSMutableArray alloc] initWithCapacity:5];
        [_delegates addObject:self];
        _metadata = [[NSMutableDictionary alloc] init];
        _registrationCompletionBlock = completion;
        dispatch_async(_remoteOperationsQueue, ^{ [self checkAndUpdateRegistration];});
    }
    return self;
}

- (void)checkAndUpdateRegistration {
    __block NSString *lpId = nil;
    NSDictionary *accountInfo = [self loadAccountInfo];
    if (accountInfo[LP_ID_KEY] != nil) {
        lpId = accountInfo[LP_ID_KEY];
        //Use the stored LP ID temporarily until we get a response from the server
        _lpId = lpId;
    }
    NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithCapacity:5];
    headers[APPLICATION_ID_KEY] = _applicationId;
    if (_correlationId != nil){
        headers[CORRELATION_ID_KEY] = _correlationId;
    }
    headers[DEVICE_TYPE_KEY] = @"ios";
    if (_deviceId != nil){
        headers[DEVICE_ID_KEY] = _deviceId;
    }
    if(lpId == nil){
        [self updateClientHeaders:headers];
        [self newRegistration:_correlationId withDeviceId:_deviceId];
    }else{
        headers[LP_ID_KEY] = lpId;
        [self updateClientHeaders:headers];
        NSString *url = [NSString stringWithFormat:@"api/register/%@/%@/", _applicationId, lpId];
        [_lpClient GET:url withDictionary:nil didComplete:^(NSDictionary *results, BOOL success) {
            if(success){
                NSString *error_code = results[ERROR_CODE_KEY];
                if(error_code != nil && [error_code isEqualToString:ERR_CODE_REG_1001]){
                    LPLOG(@"Registration [%@] missing. Creating a new registration", lpId);
                    [self newRegistration:_correlationId withDeviceId:_deviceId];
                }else{
                    LPLOG(@"Found registration: [%@]", results);
                    _lpId = results[LP_ID_KEY];
                    [self updateRegistration:_lpId withCorrelationId:_correlationId withDeviceId:_deviceId withMetadata:nil];
                }
                [self startRegionMonitors];
            }
        }];
    }
}


- (NSDictionary *)loadAccountInfo {
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"LumenPathAccountInfo" accessGroup:nil];
    NSString *lp_account = [keychainItem objectForKey:(__bridge id) (kSecValueData)];
    NSData *data = [lp_account dataUsingEncoding:NSUTF8StringEncoding];
    LPLOG(@"KEYCHAIN %@", lp_account);
    NSError *error;
    if (lp_account != nil && [lp_account length] > 0) {
        NSDictionary *accountInfo = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        if (accountInfo != nil) {
            return accountInfo;
        } else {
            LPLOG(@"Unable to load account info:[%@]", error);
        }
    }
    return @{};
}

- (void)addListener:(id <LPBeaconManagerListener>)listener {
    @synchronized(_delegates){
        for(id l in _delegates){
            if(l == listener){
                return;
            }
        }
        [_delegates addObject:listener];
    }
}

- (void)monitorRegion:(NSString *)proximityUUID andMajor:(NSInteger)major andMinor:(NSInteger)minor {
    CLBeaconRegion *region= [self createBeaconRegionWithUUID:proximityUUID withMajor:major withMinor:minor];
    region.notifyOnEntry = YES;
    region.notifyOnExit = YES;
    region.notifyEntryStateOnDisplay = YES;
    [_beaconsUnderWatch addObject: [LPSensor keyFromBeacon:(id) region]];
    if(![self isLocationUseAuthorized]){
        return;
    }
    [self.locationManager startMonitoringForRegion:region];
    [self.locationManager requestStateForRegion:region];
}

- (CLBeaconRegion *)createBeaconRegionWithUUID:(NSString *)proximityUUID withMajor:(NSInteger)major withMinor:(NSInteger)minor {
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString: proximityUUID];
    CLBeaconRegion *region;
    NSString *identifier = [NSString stringWithFormat:@"com.lumenpath.%@_%d_%d", proximityUUID, (int) major, (int)minor];
    if (major == -1 && minor == -1){
        region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:identifier];
    } else{
        region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:(CLBeaconMajorValue) major minor:(CLBeaconMinorValue) minor identifier:identifier];
    }
    return region;
}

- (void)stopRegionMonitoring:(NSString *)proximityUUID andMajor:(NSInteger)major andMinor:(NSInteger)minor {
    CLBeaconRegion *region= [self createBeaconRegionWithUUID:proximityUUID withMajor:major withMinor:minor];
    [self.locationManager stopMonitoringForRegion:region];
}

- (void)findSensorsByLocation:(double)latitude andLongitude:(double)longitude didComplete:(void (^)(NSArray *sensors, BOOL success))completion{
    NSString *url = [NSString stringWithFormat:@"api/sensor/nearest/location/%f/%f/?radius=%d", latitude, longitude, kDefaultSearchRadiusInMeters];
    [_lpClient GET: url withDictionary:nil didComplete:^(NSDictionary *results, BOOL success) {
        LPLOG(@"Sensors:%@", results);
        if(results != nil){
            NSMutableArray *sensors= [self parseSensorsFromJSON:results];
            completion(sensors, YES);
        } else{
            completion(nil, NO);
        }
    }];
}


- (NSMutableArray *)parseSensorsFromJSON:(NSDictionary *)results withKey:(NSString *)key {;
    NSArray *jsonSensors = results[key];
    NSMutableArray *sensors = [[NSMutableArray alloc] init];
    for(NSDictionary *jsonSensor in jsonSensors){
        LPSensor *sensor = [self parseSensorFromJson:jsonSensor];
        [sensors addObject:sensor];
    }
    return sensors;
}


- (NSMutableArray *)parseSensorsFromJSON:(NSDictionary *)results {
    return [self parseSensorsFromJSON:results withKey:@"sensors"];
}

- (LPSensor *)parseSensorFromJson:(NSDictionary *)jsonSensor {
    int major = [(NSString *) jsonSensor[@"major_version"] intValue];
    int minor = [(NSString *) jsonSensor[@"minor_version"] intValue];
    NSArray *location = jsonSensor[@"location"];
    NSArray *tags = jsonSensor[@"tags"];
    if(tags == nil || [tags count] == 0){
        tags = @[];
    }
    NSDictionary *metadata = [jsonSensor objectForKey:@"metadata"];
    if(metadata == nil){
        metadata = @{};
    }
    LPSensor *sensor = [[LPSensor alloc] initWithUUID:jsonSensor[@"uuid"]
                                             andMajor:@(major)
                                             andMinor:@(minor) andTags:tags andMetadata:metadata];
    if(location != nil){
        double lat;
        double lng;
        if ([location[0] isKindOfClass:[NSString class]]) {
            lat = [(NSString *) location[0] doubleValue];
            lng = [(NSString *) location[1] doubleValue];
        } else {
            lat = [location[0] doubleValue];
            lng = [location[1] doubleValue];
        }
        sensor.location = CLLocationCoordinate2DMake(lat, lng);
    }
    
    return sensor;
}

- (NSDictionary *)parseAssetCollectionsFromJson:(NSDictionary *)json {
    NSDictionary *assetCollections = [[NSMutableDictionary alloc]init];
    for(NSString *collection in [json allKeys]){
        NSMutableArray *assets = [[NSMutableArray alloc] init];
        for(NSDictionary *jsonAsset in [[json objectForKey:collection] objectForKey:@"assets"]){
            [assets addObject:[[LPAsset alloc] initWithDictionary:jsonAsset]];
        }
        [assetCollections setValue:assets forKey:collection];
    }
    return assetCollections;
}


- (NSArray *)parseAssetsFromJson:(NSDictionary *)json {
    NSMutableArray *assets = [[NSMutableArray alloc] init];
    NSArray *assetsJson = [json objectForKey:@"assets"];
    for(NSDictionary *jsonAsset in assetsJson){
        [assets addObject:[[LPAsset alloc] initWithDictionary:jsonAsset]];
    }
    return assets;
}

-(void) findSensorsByCountry:(NSString *)country
                         andState:(NSString *)state
                          andCity:(NSString *)city
                        andStreet:(NSString *)street
                      didComplete:(void (^)(NSArray *sensors, BOOL success))completion{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:5];
    [self setDictionaryKeyIfNotNil:@"country" withValue:country params:params];
    [self setDictionaryKeyIfNotNil:@"state" withValue:state params:params];
    [self setDictionaryKeyIfNotNil:@"city" withValue:city params:params];
    [self setDictionaryKeyIfNotNil:@"street" withValue:street params:params];

    [_lpClient GET: @"api/sensor/nearest/address/" withDictionary:params didComplete:^(NSDictionary *results, BOOL success) {
        LPLOG(@"Sensors:%@", results);
        if(results != nil){
            NSMutableArray *sensors= [self parseSensorsFromJSON:results];
            completion(sensors, YES);
        } else{
            completion(nil, NO);
        }
    }];
}

- (void)setDictionaryKeyIfNotNil:(NSString *)key withValue:(NSString *)value params:(NSMutableDictionary *)params {
    if(value != nil){
        params[key] = value;
    }
}

- (void) findSensorsByPostalCode:(NSString *)postalCode
                     didComplete:(void (^)(NSArray *sensors, BOOL success))completion{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:5];
    [self setDictionaryKeyIfNotNil:@"postal_code" withValue:postalCode params:params];

    [_lpClient GET: @"api/sensor/nearest/address/" withDictionary:params didComplete:^(NSDictionary *results, BOOL success) {
        LPLOG(@"Sensors:%@", results);
        if(results != nil){
            NSMutableArray *sensors= [self parseSensorsFromJSON:results];
            completion(sensors, YES);
        } else{
            completion(nil, NO);
        }
    }];
}

- (void)findSensorsByMetdata:(NSString *)ns
                      andKey:(NSString *)key
                    andValue:(NSString *)value
                 didComplete:(void (^)(NSArray *sensors, BOOL success))completion{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:5];
    [self setDictionaryKeyIfNotNil:@"key" withValue:key params:params];
    [self setDictionaryKeyIfNotNil:@"value" withValue:value params:params];
    NSString *namespaceEncoded =[ns stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"api/meta/sensor/%@/list/%@/", _applicationId, namespaceEncoded];
    
    [_lpClient GET:url withDictionary:params didComplete:^(NSDictionary *results, BOOL success) {
        LPLOG(@"Sensors:%@", results);
        if(results != nil){
            NSMutableArray *sensors= [self parseSensorsFromJSON:results];
            completion(sensors, YES);
        } else{
            completion(nil, NO);
        }
    }];

    
}


- (void) getSensor:(NSString *)uuid
               andMajor:(NSInteger)major
               andMinor:(NSInteger)minor
            didComplete:(void (^)(LPSensor *sensor, BOOL success))completion {
    NSString *sensorKey = [self keyFromUUID: uuid andMajor: major andMinor: minor];
    LPSensor *cachedSensor = [_beaconCache objectForKey: sensorKey];
    
    if(cachedSensor != nil && ![self hasCachedSensorExpired:cachedSensor]){
        NSLog(@"Found cached sensor for key: [%@]", sensorKey);
        if(completion != nil){
            completion(cachedSensor, YES);
        }
    }else{
        NSLog(@"No cached sensor for key: [%@]. Fetching data from server", sensorKey);
        NSString *url = [NSString stringWithFormat:@"api/sensor/%@/%li/%li/", uuid, (long)major, (long)minor];
        [_lpClient GET: url withDictionary:nil didComplete:^(NSDictionary *results, BOOL success) {
            LPLOG(@"Sensors:%@", results);
            if(results != nil){
                LPSensor *sensor = [self parseSensorFromJson:results];
                [_beaconsCacheLock lock];
                LPSensor *cachedSensor = [_beaconCache objectForKey:sensorKey];
                sensor.hitCount = cachedSensor.hitCount;
                sensor.lastUpdated = @(CFAbsoluteTimeGetCurrent());
                sensor.lastSeen = cachedSensor.lastSeen;
                NSLog(@"Setting sensor :[%@] for key: [%@]. Fetching data from server", sensor, sensorKey);
                [_beaconCache setObject:sensor forKey:sensorKey];
                [_beaconsCacheLock unlock];
                if(completion != nil){
                    completion(sensor, YES);
                }
            } else{
                if(completion != nil) {
                    completion(nil, NO);
                }
            }
        }];
    }
}

- (void)getSensorAssets:(NSString *)uuid
               andMajor:(NSInteger)major
               andMinor:(NSInteger)minor
            didComplete:(void (^)(NSDictionary *, BOOL))completion{
    NSString *url = [NSString stringWithFormat:@"api/asset/%@/sensor/%@/%li/%li/collections/", _applicationId, uuid, (long)major, (long)minor];
    [_lpClient GET: url withDictionary:nil didComplete:^(NSDictionary *results, BOOL success) {
//        LPLOG(@"Sensor Asset Collections:%@", results);
        if(results != nil){
            NSDictionary *assetCollections = [self parseAssetCollectionsFromJson:results];
            completion(assetCollections, YES);
        } else{
            completion(nil, NO);
        }
    }];
}

- (void)getApplicationAssets:(void (^)(NSDictionary *, BOOL))completion{
    NSString *url = [NSString stringWithFormat:@"api/asset/%@/collections/", _applicationId];
    [_lpClient GET: url withDictionary:nil didComplete:^(NSDictionary *results, BOOL success) {
//        LPLOG(@"Application Asset Collections:%@", results);
        if(results != nil){
            NSDictionary *assetCollections = [self parseAssetCollectionsFromJson:results];
            completion(assetCollections, YES);
        } else{
            completion(nil, NO);
        }
    }];
}

- (void)getApplicationAsset:(NSString *)assetId didComplete:(void (^)(LPAsset *, BOOL))completion{
    NSString *url = [NSString stringWithFormat:@"api/asset/%@/asset/%@/", _applicationId, assetId];
    [_lpClient GET: url withDictionary:nil didComplete:^(NSDictionary *results, BOOL success) {
//        LPLOG(@"Application Asset :%@", results);
        if(results != nil){
            LPAsset *asset = [[LPAsset alloc] initWithDictionary:results];
            completion(asset, YES);
        } else{
            completion(nil, NO);
        }
    }];
}

- (void)getSensorAssets:(NSString *)uuid
               andMajor:(NSInteger)major
               andMinor:(NSInteger)minor
          andCollection:(NSString *)collection
            didComplete:(void (^)(NSArray *, BOOL))completion{
    NSString *url = [NSString stringWithFormat:@"api/asset/%@/sensor/%@/%li/%li/collection/%@/", _applicationId, uuid, (long)major, (long)minor, collection];
    [_lpClient GET: url withDictionary:nil didComplete:^(NSDictionary *results, BOOL success) {
//        LPLOG(@"Asset Collections:%@", results);
        if(results != nil){
            NSArray *assets = [self parseAssetsFromJson:results];
            completion(assets, YES);
        } else{
            completion(nil, NO);
        }
    }];
}

- (void)getApplicationAssets:(NSString *)collection didComplete:(void (^)(NSArray *, BOOL))completion{
    NSString *url = [NSString stringWithFormat:@"api/asset/%@/collection/%@/", _applicationId, collection];
    [_lpClient GET: url withDictionary:nil didComplete:^(NSDictionary *results, BOOL success) {
//        LPLOG(@"Application Asset Collections:%@", results);
        if(results != nil){
            NSArray *assets = [self parseAssetsFromJson:results];
            completion(assets, YES);
        } else{
            completion(nil, NO);
        }
    }];

}

- (void)beaconManager:(id)beaconManager didBecomeActive:(NSArray *)beacons {
    for(LPSensor *sensor in beacons){
        if(_eventQueue != nil) {
            LPEvent *event = [[LPEvent alloc] initWithSensor:sensor withEventType:Enter withCorrelationId:_correlationId withLpId: _lpId];
            [_eventQueue pushEvent:event];
            if([_beaconCache objectForKey:[self keyFromSensor:sensor]] == nil){
                [self fetchSensor: sensor];
            }
        }
    }
}

- (void)beaconManager:(id)beaconManager didBecomeInactive:(NSArray *)beacons {
    for(LPSensor *sensor in beacons){
        if(_eventQueue != nil) {
            LPEvent *event = [[LPEvent alloc] initWithSensor:sensor withEventType:Exit withCorrelationId:_correlationId withLpId: _lpId];
            [_eventQueue pushEvent:event];
        }
    }
}

- (void)beaconManager:(id)beaconManager didChange:(NSArray *)beacons {
    for(LPSensor *sensor in beacons){
        if(_eventQueue != nil){
            LPEvent *event = [[LPEvent alloc] initWithSensor:sensor withEventType:Active withCorrelationId:_correlationId withLpId: _lpId];
            [_eventQueue pushEvent:event];
            if([self hasCachedSensorExpired: sensor]){
                [self fetchSensor: sensor];
            }
        }
    }
}

- (NSString *) keyFromSensor:(LPSensor *)sensor{
    return [NSString stringWithFormat:@"%@_%i_%i", sensor.uuid, sensor.major.intValue, sensor.minor.intValue];
}


- (NSString *) keyFromUUID:(NSString *)uuid andMajor:(NSInteger)major andMinor:(NSInteger)minor{
    return [NSString stringWithFormat:@"%@_%li_%li", uuid, (long)major, (long)minor];
}

                
- (BOOL) hasCachedSensorExpired:(LPSensor *)sensor{
    LPSensor *cachedSensor = [_beaconCache objectForKey:[self keyFromSensor:sensor]];
    if(cachedSensor == nil){
        return YES;
    }
    NSNumber *now = @(CFAbsoluteTimeGetCurrent());
    long age = [now longValue] - [[cachedSensor lastUpdated] longValue];
    return age > kDefaultCacheExpiryInSeconds;
}



- (void) fetchSensor:(LPSensor *)sensor{
    [self getSensor:sensor.uuid andMajor:sensor.major.intValue andMinor:sensor.minor.intValue didComplete:^(LPSensor *newSensor, BOOL success) {}];
}

/*
 *  locationManager:didEnterRegion:
 *
 *  Discussion:
 *    Invoked when the user enters a monitored region.  This callback will be invoked for every allocated
 *    CLLocationManager instance with a non-nil delegate that implements this method.
 */
- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    if([region isKindOfClass:[CLBeaconRegion class]]){
        [self.locationManager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
    }
}
/*
 *  locationManager:didExitRegion:
 *
 *  Discussion:
 *    Invoked when the user exits a monitored region.  This callback will be invoked for every allocated
 *    CLLocationManager instance with a non-nil delegate that implements this method.
 */
- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region{
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
//    LPLOG(@"Region: %@ State:", region);
    if(state == CLRegionStateInside){
        if([region isKindOfClass:[CLBeaconRegion class]]){
            [self.locationManager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
            CLBeaconRegion *beaconRegion = (CLBeaconRegion *) region;
            if([beaconRegion major] != nil && [beaconRegion minor] != nil){
                if([[manager monitoredRegions] count] < 20){
                    [self monitorRegion:[[beaconRegion proximityUUID] UUIDString] andMajor:[[beaconRegion major] integerValue] andMinor:[[beaconRegion minor] integerValue]];
                }
            }
        }
    }
}

/*
 *  locationManager:didRangeBeacons:inRegion:
 *
 *  Discussion:
 *    Invoked when a new set of beacons are available in the specified region.
 *    beacons is an array of CLBeacon objects.
 *    If beacons is empty, it may be assumed no beacons that match the specified region are nearby.
 *    Similarly if a specific beacon no longer appears in beacons, it may be assumed the beacon is no longer received
 *    by the device.
 */
- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    __block NSMutableDictionary *lpBeacons = [[NSMutableDictionary alloc]initWithCapacity:[beacons count]];
    for(CLBeacon *beacon in beacons){
        NSString *key = [LPSensor keyFromBeacon:(id) beacon];
        LPSensor *lpSensor = [[LPSensor alloc] initWithBeacon:(id) beacon];
        [lpBeacons setValue:lpSensor forKey:key];
        //Fetch and fill the cache
        [self getSensor:[lpSensor uuid] andMajor: [[lpSensor major] integerValue] andMinor: [[lpSensor minor] integerValue] didComplete: nil];
    }
    dispatch_async(_localOperationsQueue, ^{
//    LPLOG(@"Regions scanned %d", [beacons count]);
        NSMutableArray *activeBeacons = [NSMutableArray array];
        NSMutableArray *changedBeacons = [NSMutableArray array];
        for(NSString *key in [lpBeacons allKeys]){
//            LPLOG(@"Update for key:%@", key);
            if (![_beaconsUnderWatch containsObject:key]) {
                LPLOG(@"key not under watch:%@", key);
                LPSensor *lpSensor = [lpBeacons objectForKey:key];
                lpSensor.location = self.currentLocation;
                [lpSensor touch];
                [_beaconsCacheLock lock];
                [_beaconCache setObject:lpSensor forKey:key];
                [_beaconsCacheLock unlock];
                [_beaconsUnderWatch addObject:key];
//                LPLOG(@"Adding new beacon with lastSeen %@, %p", [lpSensor lastSeen], lpSensor);
                [activeBeacons addObject:lpSensor];
//                LPLOG(@"new Beacon cache with lastSeen %@, %p", [[_beaconCache objectForKey:key] lastSeen], lpSensor);
            }else{
                LPSensor *lpSensor = _beaconCache[key];
                if(lpSensor == nil){
                    LPLOG(@"Cache miss for %@, using default beacon", key);
                    lpSensor = [lpBeacons objectForKey:key];
                }else{
                    LPLOG(@"Cache hit for %@", key);
                }
                lpSensor.location = self.currentLocation;
                if([LPBeaconManager shouldActivateBeacon: lpSensor]){
//                    LPLOG(@"Adding beacon with lastSeen %@, %p", [lpSensor lastSeen], lpSensor);
                    [activeBeacons addObject:lpSensor];
                }else{
                    [changedBeacons addObject:lpSensor];
                }
                [lpSensor touch];
                NSLog(@"New hitCount %ld, %@", (long)[[lpSensor hitCount] integerValue], [NSThread currentThread]);
            }
        }
        NSMutableArray *inactiveBeacons = [NSMutableArray array];
        [self buildInactiveBeaconsList:inactiveBeacons];

        for(id<LPBeaconManagerListener> delegate in _delegates){
            if([inactiveBeacons count] > 0){
                [delegate beaconManager:self didBecomeInactive:inactiveBeacons];
            }
            if([activeBeacons count] > 0){
                [delegate beaconManager:self didBecomeActive:activeBeacons];
            }
            if([changedBeacons count] > 0){
                [delegate beaconManager:self didChange:changedBeacons];
            }
        }
        if(_eventQueue != nil){
            [_eventQueue flush];
        }
    });
}

- (void)buildInactiveBeaconsList:(NSMutableArray *)inactiveBeacons {
    NSNumber *now = @(CFAbsoluteTimeGetCurrent());
    for(NSString *key in _beaconsUnderWatch){
        LPSensor *beacon = [_beaconCache objectForKey:key];
        long lastSeen = [[beacon lastSeen] integerValue];
        //Have seen an update within last _maximumInactivityPeriodInSeconds
        if (lastSeen < ([now integerValue] - _maximumInactivityPeriodInSeconds)) {
            //only send inactivity event updates when inside _maximumInactivityEventPeriodInSeconds window
            if(lastSeen > ([now integerValue] - _maximumInactivityEventPeriodInSeconds)){
                //Is beacon still under watch. Only send updates for beacons that are still under watch.
                if(lastSeen > ([now integerValue] - _maximumInactiveWatchPeriodInSeconds)){
                    [inactiveBeacons addObject:beacon];
                }
            }
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    LPLOG(@"Error: %@", [error localizedDescription]);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    LPLOG(@"Current Location: %@", [locations lastObject]);
    NSUInteger objCount = [locations count];
    if (objCount > 0) {
        CLLocation *oldLocation = locations[objCount - 1];
        self.currentLocation = CLLocationCoordinate2DMake([oldLocation coordinate].latitude, [oldLocation coordinate].longitude);
    }
}

- (void)updateRegistration:(NSString *)lpId withCorrelationId:(NSString *)correlationId withDeviceId:(NSString *)deviceId withMetadata:(NSDictionary *)meta {
    __block NSMutableDictionary *registrationParams = [[NSMutableDictionary alloc] init];
    registrationParams[APPLICATION_ID_KEY] = _applicationId;
    if(correlationId != nil){
        registrationParams[CORRELATION_ID_KEY] = correlationId;
    }
    registrationParams[DEVICE_TYPE_KEY] = @"ios";
    if(deviceId != nil){
        registrationParams[DEVICE_ID_KEY] = deviceId;
    }
    if(meta != nil){
        registrationParams[@"metadata"] = meta;
    }
    NSString *url = [NSString stringWithFormat:@"api/register/%@/%@/", _applicationId, lpId];
    [_lpClient POST:url withDictionary:registrationParams didComplete:^(NSDictionary *results, BOOL success) {
        LPLOG(@"Reg Params [%@]", registrationParams);
        if(!success){
//            LPLOG("Registration Update failed");
            return;
        }
        KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"LumenPathAccountInfo" accessGroup: nil];
        NSString *lpId = results[LP_ID_KEY];
        LPLOG(@"Registration Update: %@", results);
        LPLOG(@"LPID: %@", lpId);
        NSArray *defaultSensors = [[NSMutableArray alloc] init];
        NSDictionary *appDictionary = results[@"application"];
        if (appDictionary != nil && appDictionary[@"default_sensors"] != nil) {
            defaultSensors = [self parseSensorsFromJSON:appDictionary withKey:@"default_sensors"];
        }
        [self setAccountInfoWithApplicationId:_applicationId
                            withCorrelationId:_correlationId
                                     withLpId:lpId
                           withDefaultSensors:defaultSensors
                             withKeychainItem:keychainItem];
        [self refreshClientHeaders];
        [self configureEventQueue];
        NSMutableDictionary *meta = results[@"metadata"];
        if(meta != nil){
            for(NSString *namespace in [meta allKeys]){
                NSMutableDictionary *nsMeta = meta[namespace];
                for(NSString *nsKey in [nsMeta allKeys]){
                    [self addMetadata:namespace withKey:nsKey withValue:nsMeta[nsKey]];
                }
            }
        }
        if(_registrationCompletionBlock != nil){
            _registrationCompletionBlock(@"Registration update was successfully", YES);
            _registrationCompletionBlock = nil;
        }
    }];
}

- (void)newRegistration:(NSString *)correlationId withDeviceId:(NSString *)deviceId {
    __block NSMutableDictionary *registrationParams = [[NSMutableDictionary alloc] init];
    registrationParams[APPLICATION_ID_KEY] = _applicationId;
    if(correlationId != nil){
        registrationParams[CORRELATION_ID_KEY] = correlationId;
    }
    registrationParams[DEVICE_TYPE_KEY] = @"ios";
    if(deviceId != nil){
        registrationParams[DEVICE_ID_KEY] = deviceId;
    }
    NSString *url = [NSString stringWithFormat:@"api/register/%@/", _applicationId];
    [_lpClient PUT:url withDictionary:registrationParams didComplete:^(NSDictionary *results, BOOL success) {
        if(!success){
            LPLOG("Registration failed");
            return;
        }
        KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"LumenPathAccountInfo" accessGroup: nil];
        _lpId = results[LP_ID_KEY];
            LPLOG(@"Registration: %@", results);
            LPLOG(@"LPID: %@", _lpId);
        NSArray *defaultSensors = [NSMutableArray array];
        NSDictionary *appDictionary = results[@"application"];
        if (appDictionary != nil && appDictionary[@"default_sensors"] != nil) {
            defaultSensors = [self parseSensorsFromJSON:appDictionary withKey:@"default_sensors"];
        }
        [self setAccountInfoWithApplicationId:_applicationId
                            withCorrelationId:_correlationId
                                     withLpId:_lpId
                           withDefaultSensors:defaultSensors
                             withKeychainItem:keychainItem];
        [self refreshClientHeaders];
        [self configureEventQueue];
        if(_registrationCompletionBlock != nil){
            _registrationCompletionBlock(@"New registration completed successfully", YES);
            _registrationCompletionBlock = nil;
        }
    }
    ];
}

- (void)configureEventQueue {
    NSAssert((_lpId != nil && [_lpId length] > 0), @"lp_id is required before creating a event queue");

    if(_eventQueue == nil){
        LPLOG(@"Creating event queue for lpId [%@], device_id[%@], correlationI[%@]", _lpId, _deviceId, _correlationId);
        _eventQueue = [[LPEventQueue alloc] initWithClient:_lpClient withFlushInterval:@5];
    }
}

-(void) setAccountInfoWithApplicationId:(NSString *)applicationId
                      withCorrelationId:(NSString *)correlationId
                               withLpId:(NSString *)lpId
        withDefaultSensors:(NSArray *)sensors
                       withKeychainItem:(KeychainItemWrapper *)keychainItem {
    NSMutableDictionary *accountInfo = [[NSMutableDictionary alloc] initWithDictionary: @{APPLICATION_ID_KEY :  applicationId}];
    if(correlationId != nil){
        accountInfo[CORRELATION_ID_KEY] = correlationId;
    }
    if(lpId != nil){
        accountInfo[LP_ID_KEY] = lpId;
    }
    NSMutableArray *sensorArray = [[NSMutableArray alloc] init];
    for (LPSensor *sensor in sensors) {
        sensor.lastSeen = @0;
        [sensorArray addObject:[sensor asDictionary]];
    }
    accountInfo[@"default_sensors"] = sensorArray;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:accountInfo options:NSJSONWritingPrettyPrinted error:&error];
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [keychainItem setObject: jsonString forKey:(__bridge id)(kSecValueData)];
    }
}


- (void)setDeviceId:(NSString *)deviceId {
    LPLOG(@"Setting deviceId to[%@]", deviceId);
    _deviceId = deviceId;
    [self refreshClientHeaders];

}

-(void)addMetadata:(NSString *)namespace withKey:(NSString *)key withValue:(NSString *)value{
    if (_metadata[namespace] == nil) {
        _metadata[namespace] = [[NSMutableDictionary alloc] init];
    }
    [_metadata[namespace] setObject:value forKey:key];
}


-(NSString *)getMetadata:(NSString *)namespace withKey:(NSString *)key{
    if (_metadata[namespace] != nil) {
        return _metadata[namespace][key];
    }
    return nil;
}



- (void)refreshClientHeaders {
    NSMutableDictionary *headerParams = [[NSMutableDictionary alloc] init];
    if(_applicationId != nil) {
        headerParams[APPLICATION_ID_KEY] = _applicationId;
    }
    if(_correlationId != nil) {
        headerParams[CORRELATION_ID_KEY] = _correlationId;
    }
    if(_lpId != nil) {
        headerParams[LP_ID_KEY] = _lpId;
    }
    headerParams[DEVICE_TYPE_KEY] = @"ios";
    if(_deviceId != nil) {
        headerParams[DEVICE_ID_KEY] = _deviceId;
    }
    [self updateClientHeaders:headerParams];
}

- (void)startRegionMonitors {
    NSDictionary *accountInfo = [self loadAccountInfo];
    if (accountInfo[DEFAULT_SENSORS_KEY] != nil) {
        NSArray *sensors = accountInfo[DEFAULT_SENSORS_KEY];
        for (NSDictionary *sensor in sensors) {
            [self monitorRegion:sensor[@"uuid"] andMajor:[sensor[@"major_version"] integerValue] andMinor:[sensor[@"minor_version"] integerValue]];
        }
    }
}


- (void)updateClientHeaders:(NSMutableDictionary *)headers{
    [_lpClient addHeader:LP_DEVICE_APPLICATION_ID_HEADER value:headers[APPLICATION_ID_KEY]];
    [_lpClient addHeader:LP_DEVICE_CORRELATION_ID_HEADER value:headers[CORRELATION_ID_KEY]];
    [_lpClient addHeader:LP_DEVICE_LP_ID_HEADER value:headers[LP_ID_KEY]];
    [_lpClient addHeader:LP_DEVICE_ID_HEADER value:headers[DEVICE_ID_KEY]];
    [_lpClient addHeader:LP_DEVICE_TYPE_HEADER value:headers[DEVICE_TYPE_KEY]];
}


- (void)updateRegistration {
    dispatch_async(_remoteOperationsQueue, ^{
    [self updateRegistration:_lpId withCorrelationId:_correlationId withDeviceId:_deviceId withMetadata:_metadata];
    });
}

- (NSString *)getCorrelationId {
    return _correlationId;
}


- (void)setCorrelationId:(NSString *)correlationId {
    _correlationId = correlationId;
}

- (void)clearMetadata {
    [_metadata removeAllObjects];
}

- (void)clearRegistration {
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"LumenPathAccountInfo" accessGroup: nil];
    _lpId = nil;
    NSMutableDictionary *accountInfo = [[NSMutableDictionary alloc] initWithDictionary: @{APPLICATION_ID_KEY :  _applicationId}];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:accountInfo options:NSJSONWritingPrettyPrinted error:&error];
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [keychainItem setObject: jsonString forKey:(__bridge id)(kSecValueData)];
    }
}

+ (BOOL) shouldActivateBeacon:(LPSensor *)sensor{
    long lastSeen = [[sensor lastSeen] longValue];
    long now = [@(CFAbsoluteTimeGetCurrent()) longValue];
//    LPLOG(@"Time since lastSeen:%ld", (now  - lastSeen));
    return (now  - lastSeen)  > kDefaultDelayBeforeSensorReactivationInSeconds;
}

- (BOOL) isLocationUseAuthorized{
    return [self isLocationUseAuthorizedStatus: [CLLocationManager authorizationStatus]];
}


- (BOOL) isLocationUseAuthorizedStatus: (CLAuthorizationStatus) status{
    return status == kCLAuthorizationStatusAuthorizedWhenInUse
    ||status == kCLAuthorizationStatusAuthorizedAlways
    ||status == kCLAuthorizationStatusAuthorized;
}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if([self isLocationUseAuthorizedStatus:status]){
        [self.locationManager startUpdatingLocation];
        if([_beaconsUnderWatch count] == 0){
            return;
        }
        NSSet *beacons = [[NSSet alloc] initWithSet:_beaconsUnderWatch];
        for(NSString *key in beacons){
            NSString *uuid = [LPSensor uuidFromKey:key];
            NSInteger major = [LPSensor majorFromKey:key];
            NSInteger minor = [LPSensor minorFromKey:key];
            [self monitorRegion:uuid andMajor:major andMinor:minor];
        }
    }
}
@end
