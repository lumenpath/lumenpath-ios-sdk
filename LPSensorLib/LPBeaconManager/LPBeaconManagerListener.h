//
//  LPBeaconManagerListener.h
//  LumenPath
//
//  Created by Alok Singh on 5/15/14.
//  Copyright (c) 2014 LumenPath Inc. All rights reserved.
//

/**
* Classes that implement `LPBeaconManagerListener` interface can be registered with `LPBeaconManager` to receive
* iBeacon activity events.
*
*/

@protocol LPBeaconManagerListener <NSObject>

/**
* Method called when one more iBeacons become active
* @param beacons an array of iBeacons
*/
- (void)beaconManager:(id)beaconManager didBecomeActive:(NSArray *)beacons;

/**
* Method called when one more iBeacons become inactive
* @param beacons an array of iBeacons
*/
- (void)beaconManager:(id)beaconManager didBecomeInactive:(NSArray *)beacons;

/**
* Method called when one more iBeacons changes state.
* @param beacons an array of iBeacons
*/
- (void)beaconManager:(id)beaconManager didChange:(NSArray *)beacons;

@end
