//
// Created by Alok Singh on 5/18/14.
// Copyright (c) 2014 LumenPath Inc. All rights reserved.
//

#import "LPSensor.h"
#import "LPClient.h"
#import "LPGlobal.h"
@implementation LPSensor

@synthesize lastUpdated;
@synthesize lastSeen;
@synthesize hitCount;

@end

@implementation LPSensor(LPSensorPrivate)

    NSString *_uuid;
    NSNumber *_major;
    NSNumber *_minor;
    NSString *_key;
    NSDictionary *_metadata;

- (id)initWithBeacon:(CLBeaconRegion *)beaconRegion {
    if (self = [super init]) {
        _uuid = beaconRegion.proximityUUID.UUIDString;
        if (beaconRegion.major.integerValue > 0) {
            _major = @([[beaconRegion major] integerValue]);
        } else {
            _major = @0;
        }
        if (beaconRegion.minor.integerValue > 0) {
            _minor = @([[beaconRegion minor] integerValue]);
        } else {
            _minor = @0;
        }
        _key = [NSString stringWithFormat:@"%@_%i_%i", self.uuid, self.major.intValue, self.minor.intValue];
        lastSeen = @0;
        lastUpdated = @0;
        hitCount = @0;
        _metadata = @{};
//        LPLOG(@"New sensor object for key: %@ %p", _key, self);
    }
    return self;
}

- (id)initWithUUID:(NSString *)uuid andMajor:(NSNumber *)major andMinor:(NSNumber *)minor {
    if (self = [super init]) {
        _uuid = [NSString stringWithString:uuid];
        _major = @([major integerValue]);
        _minor = @([minor integerValue]);
        _key = [NSString stringWithFormat:@"%@_%i_%i", self.uuid, self.major.intValue, self.minor.intValue];
        self.lastUpdated = @0;
        self.lastSeen = 0;
        self.hitCount = @0;
        _tags = @[];
        _metadata = @{};
//        LPLOG(@"New sensor object for key: %@ %p", _key, self);
    }
    return self;
}

- (id)initWithUUID:(NSString *)uuid andMajor:(NSNumber *)major andMinor:(NSNumber *)minor andTags:(NSArray *) tags andMetadata:(NSDictionary *)metadata{
    if (self = [super init]) {
        _uuid = [[NSString alloc] initWithString:[uuid copy]];
        _major = @([major integerValue]);
        _minor = @([minor integerValue]);
        _key = [NSString stringWithFormat:@"%@_%i_%i", self.uuid, self.major.intValue, self.minor.intValue];
        self.lastUpdated = @0;
        self.lastSeen = 0;
        self.hitCount = @0;
        _tags = [[NSArray alloc]initWithArray:tags];
        _metadata = [[NSDictionary alloc]initWithDictionary:metadata];
        LPLOG(@"New sensor object %@ for key: %@ %p", self, _key, self);
    }
    return self;
}


- (void)touch {
    if([[self lastSeen] integerValue] < 1){
        self.hitCount =  @(1);
    }
    NSNumber *now = @(CFAbsoluteTimeGetCurrent());
    if([now doubleValue] - [[self lastSeen] doubleValue]  > kDefaultScanDelayInSeconds){
        self.hitCount = @(1);
    } else{
        self.hitCount = @([[self hitCount] intValue] + 1);
    }
    LPLOG(@"Setting hitcount :%@ for key: %@", hitCount, _key);
    self.lastSeen = now;
}

- (NSString *)key {
    return _key;
}

+ (NSString *)keyFromBeacon:(CLBeaconRegion *)beaconRegion {
    int minor = -1;
    int major = -1;
    if(beaconRegion.major != nil){
        major = beaconRegion.major.intValue;
    }
    if(beaconRegion.minor != nil){
        minor = beaconRegion.minor.intValue;
    }
    return [NSString stringWithFormat:@"%@_%i_%i", beaconRegion.proximityUUID.UUIDString, major, minor];
}

+ (NSString *)uuidFromKey:(NSString *)beaconKey {
    NSArray* parts = [beaconKey componentsSeparatedByString: @"_"];
    return [parts objectAtIndex:0];
}


+ (NSInteger) majorFromKey:(NSString *)beaconKey {
    NSArray* parts = [beaconKey componentsSeparatedByString: @"_"];
    return [[parts objectAtIndex:1] integerValue];
}

+ (NSInteger) minorFromKey:(NSString *)beaconKey {
    NSArray* parts = [beaconKey componentsSeparatedByString: @"_"];
    return [[parts objectAtIndex:2] integerValue];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"LPSensor: UUID=%@ major=%@ minor=%@ lastSeen =%@ lastUpdate =%@ tags[%@] hitCount =%@", _uuid, _major, _minor, lastSeen, lastUpdated, _tags, hitCount];
}

- (NSString *)metadataValue:(NSString *)namespace andKey:(NSString *)key{
    if([_metadata objectForKey:namespace] == nil){
        return nil;
    }
    return [[_metadata objectForKey:namespace] objectForKey: key];
}

- (NSString *)asJson {
    return [LPClient dictionaryAsJson:[self asDictionary]];
}

- (NSDictionary *)asDictionary {
    NSArray *location = @[@(self.location.latitude), @(self.location.longitude)];
    NSDictionary *data = @{@"uuid" : _uuid,
            @"major_version" : _major,
            @"minor_version" : _minor,
            @"last_seen" : (lastSeen == nil) ? 0 : lastSeen,
            @"last_updated" : (lastUpdated == nil) ? 0 : lastUpdated,
            @"tags" : (self.tags == nil ? [[NSArray alloc] init] : self.tags),
            @"location" : location
    };
    return data;
}

@end