//
// Created by Alok Singh on 5/18/14.
// Copyright (c) 2014 LumenPath Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLBeaconRegion.h>

#define kDefaultScanDelayInSeconds 5

@interface LPSensor :  NSObject <NSCopying>

@property(nonatomic, assign) CLLocationCoordinate2D location;
@property(readonly, strong) NSArray *tags;
@property(atomic, strong) NSNumber *lastSeen;
@property(atomic, strong) NSNumber *hitCount;
@property(atomic, strong) NSNumber *lastUpdated;
@property(readonly, strong) NSString *uuid;
@property(readonly, strong) NSNumber *major;
@property(readonly, strong) NSNumber *minor;
@property(readonly, strong) NSString *key;
@property(readonly, strong) NSDictionary *metadata;

- (NSString *)metadataValue:(NSString *)namespace andKey:(NSString *)key;

@end


@interface LPSensor (LPSensorPrivate)

- (id)initWithBeacon:(CLBeaconRegion *)beaconRegion;

- (id)initWithUUID:(NSString *)uuid andMajor:(NSNumber *)major andMinor:(NSNumber *)minor;

- (id)initWithUUID:(NSString *)uuid andMajor:(NSNumber *)major andMinor:(NSNumber *)minor andTags:(NSArray *) tags andMetadata:(NSDictionary *)metadata;

- (void)touch;

+ (NSString *)keyFromBeacon:(CLBeaconRegion *)beaconRegion;
+ (NSString *)uuidFromKey:(NSString *)beaconKey;
+ (NSInteger) majorFromKey:(NSString *)beaconKey;
+ (NSInteger) minorFromKey:(NSString *)beaconKey;


- (NSString *)asJson;

- (NSDictionary *)asDictionary;

@end