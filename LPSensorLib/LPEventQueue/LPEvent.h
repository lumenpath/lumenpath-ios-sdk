//
//  LPEvent.h
//  LumenPath
//
//  Created by Alok Singh on 5/18/14.
//  Copyright (c) 2014 LumenPath Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    Enter,
    Exit,
    Active
} LPEventType;


@class LPSensor;

@interface LPEvent : NSObject

- (id)initWithSensor:(LPSensor *)sensor withEventType:(LPEventType)eventType withCorrelationId:(NSString *)correlationId withLpId:(NSString *)lpId;

- (LPSensor *)sensor;

- (LPEventType)eventType;

- (NSString *)asJson;

- (NSDictionary *)asDictionary;
@end
