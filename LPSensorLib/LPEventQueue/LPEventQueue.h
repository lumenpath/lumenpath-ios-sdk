//
//  LPEventQueue.h
//  LumenPath
//
//  Created by Alok Singh on 5/18/14.
//

#import <Foundation/Foundation.h>

@class LPEvent;
@class KeychainItemWrapper;
@class LPClient;

@interface LPEventQueue : NSObject

- (id)initWithClient:(LPClient *)client withFlushInterval:(NSNumber *)flushInterval;

- (void)pushEvent:(LPEvent *)event;

- (void)flush;

- (LPClient *)lpClient;

@property (nonatomic, strong) dispatch_queue_t operationsQueue;

@end
