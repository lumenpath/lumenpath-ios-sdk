//
//  LPEvent.m
//  LumenPath
//
//  Created by Alok Singh on 5/18/14.
//  Copyright (c) 2014 LumenPath Inc. All rights reserved.
//

#import "LPEvent.h"
#import "LPSensor.h"
#import "LPClient.h"

@implementation LPEvent {
@private
    LPSensor *_sensor;
    LPEventType _eventType;
    NSString *_correlationId;
    NSTimeInterval _timeStamp;
    NSString *_lpId;
}
- (id)initWithSensor:(LPSensor *)sensor withEventType:(LPEventType)eventType withCorrelationId:(NSString *)correlationId withLpId:(NSString *)lpId {
    self = [super init];
    if (self) {
        _sensor = sensor;
        _eventType = eventType;
        _correlationId = correlationId;
        _lpId = lpId;
        _timeStamp = [[NSDate date] timeIntervalSince1970];
    }
    return self;
}

- (LPSensor *)sensor {
    return _sensor;
}

- (LPEventType)eventType {
    return _eventType;
}

- (NSString *)asJson {
    return [LPClient dictionaryAsJson:[self asDictionary]];
}

- (NSDictionary *)asDictionary {
    return @{
            @"sensor" : [_sensor asDictionary],
            @"event_type" : [self eventTypeName],
            @"timestamp" : @((long) _timeStamp),
            @"correlation_id" : (_correlationId == nil? @"" : _correlationId),
            @"lp_id" : _lpId
    };
}


- (NSString *)description {
    return [NSString stringWithFormat:@"LPEvent: sensor=%@ type=%d correlationId=%@ lpId=%@", _sensor, _eventType, _correlationId, _lpId];
}

- (NSString *)eventTypeName {
    switch (_eventType) {
        case Enter:
            return @"Enter";
        case Exit:
            return @"Exit";
        case Active:
            return @"Active";
    }
    return @"Inactive";
}

@end
