//
//  LPEventQueue.m
//  LumenPath
//
//  Created by Alok Singh on 5/18/14.
//

#import "LPEventQueue.h"
#import "LPEvent.h"
#import "LPSensor.h"
#import "LPGlobal.h"
#import "LPClient.h"

#define kDefaultMaxEventsInQueue 100
#define kDefaultMinPostingDelayInSeconds 5

@implementation LPEventQueue{
@private
    long _flushInterval;
    long _eventsInQueue;
    double _lastPostingTime;
    NSMutableDictionary *_sensorEventDictionary;
    LPClient *_lpClient;
}

-(id) initWithClient:(LPClient *)client withFlushInterval:(NSNumber *)flushInterval{
    self = [super init];
    if(self) {
        _lpClient = client;
        _flushInterval = [flushInterval longValue] < kDefaultMinPostingDelayInSeconds ? kDefaultMinPostingDelayInSeconds : [flushInterval longValue];
        _sensorEventDictionary = [[NSMutableDictionary alloc] initWithCapacity:kDefaultMaxEventsInQueue];
        _eventsInQueue = 0;
        _lastPostingTime = 0;
        _operationsQueue = dispatch_queue_create("com.lumenpath.event.EventOpsQueue", DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

- (void)pushEvent:(LPEvent *)event {
    if(([event.sensor minor]== nil) || ([event.sensor major] == nil)){
        LPLOG(@"Event sensor has no major and/or minor version. Ignoring event: %@", event);
        return;
    }
    if([event.sensor.hitCount integerValue] < 2){
        if(event.eventType == Active || event.eventType == Enter){
            LPLOG(@"Sensor detection is not reliable yet. Ignoring event: %@", event);
            return;
        }
    }
    dispatch_barrier_async(self.operationsQueue, ^{
        if (_sensorEventDictionary[[[event sensor] key]] == nil) {
            NSMutableArray *sensorEvents = [[NSMutableArray alloc] initWithCapacity:100];
            _sensorEventDictionary[[[event sensor] key]] = sensorEvents;
        }
        NSMutableArray *sensorEvents = _sensorEventDictionary[[[event sensor] key]];
        NSUInteger arraySize = [sensorEvents count];
        if(arraySize == 0){
            [sensorEvents addObject:event];
            _eventsInQueue++;
        } else{
            id lastSensorEvent = sensorEvents[arraySize - 1];
            if(((LPEvent *)lastSensorEvent).eventType == [event eventType]){
                sensorEvents[arraySize - 1] = event;
            } else{
                [sensorEvents addObject:event];
                _eventsInQueue++;
            }
        }
    });
}

- (void)flush {
    dispatch_barrier_async(self.operationsQueue, ^{
        NSNumber *now = @(CFAbsoluteTimeGetCurrent());
        if (_eventsInQueue >= kDefaultMaxEventsInQueue || (_eventsInQueue > 0 && (now.longValue > (_lastPostingTime + _flushInterval)))) {

            LPLOG(@"Flushing %ld events from queue, last post was %f seconds ago", _eventsInQueue, (now.longValue -
                    _lastPostingTime));
            NSMutableArray *eventJsons = [[NSMutableArray alloc] initWithCapacity:(NSUInteger)
                    _eventsInQueue];
            for (NSMutableArray *sensorEvents in [_sensorEventDictionary allValues]) {
                if ([sensorEvents
                        count] > 0) {
                    [eventJsons addObject:[[sensorEvents lastObject]
                            asDictionary]];
                }
                [sensorEvents
                        removeAllObjects];
            }
            _eventsInQueue = 0;
            _lastPostingTime = [@(CFAbsoluteTimeGetCurrent()) longValue];
            NSDictionary *params = @{@"events" : eventJsons};
            [_lpClient PUT:@"api/event/" withDictionary:params didComplete:nil];
        }
    });
}

- (LPClient *)lpClient {
    return _lpClient;
}


@end
