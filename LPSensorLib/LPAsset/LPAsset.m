//
//  LPAsset.m
//  LPSensorLib
//
//  Created by Alok Singh on 10/10/14.
//  Copyright (c) 2014 LumenPath Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LPAsset.h"


@implementation LPAsset {
@private
    NSString *_assetId;
    NSString *_url;
    NSString *_contentType;
    NSNumber *_size;
    NSTimeInterval _lastModified;
    NSNumber *_lastRead;
    NSMutableDictionary *_metadata;
}
+ (NSDateFormatter *) dateFormatter{
    static NSDateFormatter *_singleton;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _singleton = [[NSDateFormatter alloc] init];
        [_singleton setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    });
    
    return _singleton;
}

- (id)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if (self) {
        _assetId = [dictionary objectForKey:@"id"];
        _url = [dictionary objectForKey:@"content_url"];
        _contentType = [dictionary objectForKey:@"content_type"];
        _size = [dictionary objectForKey:@"size"];
        NSString *modifiedDate = [dictionary objectForKey:@"modified_on"];
        NSDate *date = [[LPAsset dateFormatter] dateFromString:modifiedDate];
        _lastModified = [date timeIntervalSince1970] * 1000;
        _metadata = [[NSMutableDictionary alloc] init];
        if([dictionary objectForKey:@"metadata"] != nil){
            [_metadata addEntriesFromDictionary:[dictionary objectForKey:@"metadata"]];
        }
        if([dictionary objectForKey:@"lastRead"] != nil){
            _lastRead = [dictionary objectForKey:@"lastRead"];
        }else {
            _lastRead = @0;
        }
    }
    return self;
}

- (NSString *) assetId{
    return _assetId;
}

- (NSString *) url{
    return _url;
}

- (NSString *) contentType{
    return _contentType;
}

- (NSNumber *) size{
    return _size;
}

- (long) lastModified{
    return (long)_lastModified;
}

- (long) lastRead{
    return [_lastRead longValue];
}


- (NSString *)getMetadataValue:(NSString *)key {
    if([_metadata objectForKey:@"default"] != nil){
        NSDictionary *defaults = [_metadata objectForKey:@"default"];
        if([defaults objectForKey:key] != nil){
            return [defaults objectForKey:key];
        }
    }
    return @"";
}

- (NSString *) name{
    NSString *key = @"name";
    return [self getMetadataValue:key];
}

-(void) touch{
    _lastRead = @(CFAbsoluteTimeGetCurrent());;
}

- (NSDictionary *)asDictionary{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:_assetId forKeyPath:@"id"];
    [dictionary setValue:_url forKey:@"content_url"];
    [dictionary setValue:_contentType forKey:@"content_type"];
    [dictionary setValue:_size forKey:@"size"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:_lastModified];
    
    [dictionary setValue:[[LPAsset dateFormatter] stringFromDate:date] forKey:@"modified_on"];
    if(_metadata != nil){
        [dictionary setValue:_metadata forKey:@"metadata"];
    }
    return dictionary;
}

@end
