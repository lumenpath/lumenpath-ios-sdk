//
// Created by Alok Singh on 5/18/14.
// Copyright (c) 2014 LumenPath. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LPAsset : NSObject

+ (NSDateFormatter *)dateFormatter;

- (id)initWithDictionary:(NSDictionary *)dictionary;

- (NSString *)assetId;

- (NSString *)contentType;

- (NSNumber *)size;

- (NSString *)name;

- (NSString *)url;

- (NSDictionary *)asDictionary;

- (long) lastModified;

- (long) lastRead;

- (void) touch;

@end