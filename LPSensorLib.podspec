Pod::Spec.new do |s|  
  s.name         = "LPSensorLib"
  s.version      = "0.0.5"
  s.summary      = "LumenPath Sensor Library."

  s.description  = <<-DESC
                   Library to manage iBeacons and integrate with LumenPath proximity sensor management service.
                   DESC

  s.homepage     = "https://bitbucket.org/lumenpath/lumenpath-ios-sdk"
  s.license      = 'LumenPath'
  s.author       = { "Alok Singh" => "alok@lumenpath.com" }
  s.platform     = :ios, '7.0'
  s.source       = { :git => 'https://bitbucket.org/lumenpath/lumenpath-ios-sdk.git', :tag => "v0.0.5"}
  s.source_files  = 'LPSensorLib', 'LPSensorLib/**/*.{h,m}'
  s.public_header_files = 'LPSensorLib/**/*.h'
  s.framework    = 'SystemConfiguration'
  s.requires_arc = true

end  
